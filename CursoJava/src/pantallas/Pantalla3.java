package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class Pantalla3 {

	private JFrame frame;
	private JTextField textNota1;
	private JTextField textNota2;
	private JTextField textNota3;
	private JLabel lblNota2;
	private JLabel lblNota3;
	private JLabel lblResult;
	private JLabel lblimagen;
	private JTextField textnota2;
	private JTextField textnota3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 555, 324);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Promedio de notas");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNewLabel.setBounds(95, 11, 248, 37);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNota = new JLabel("nota1");
		lblNota.setBounds(68, 76, 46, 14);
		frame.getContentPane().add(lblNota);
		
		textNota1 = new JTextField();
		textNota1.setBounds(127, 73, 86, 20);
		frame.getContentPane().add(textNota1);
		textNota1.setColumns(10);
		
		lblNota2 = new JLabel("nota2");
		lblNota2.setBounds(68, 113, 46, 14);
		frame.getContentPane().add(lblNota2);
		
		lblNota3 = new JLabel("nota3");
		lblNota3.setBounds(68, 162, 46, 14);
		frame.getContentPane().add(lblNota3);
		
		lblResult = new JLabel("");
		lblResult.setBackground(Color.ORANGE);
		lblResult.setOpaque(true);
		lblResult.setBounds(297, 136, 86, 40);
		frame.getContentPane().add(lblResult);
		
		JButton btnCalcular = new JButton("calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				float fnota1 = Float.parseFloat(textNota1.getText());
				float fnota2 = Float.parseFloat(textnota2.getText());
				float fnota3 = Float.parseFloat(textnota3.getText());
				
				float fpromedio = (fnota1+fnota2+fnota3)/3;
				if(fpromedio>=7){
				
				lblResult.setText(Float.toString(fpromedio));
				lblResult.setBackground(Color .GREEN);
				lblimagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoArriba_32px.png")));
				}
				else{ 
					lblResult.setText(Float.toString(fpromedio));
					lblResult.setBackground(Color .RED);
					lblimagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoAbajo_32px.png")));
				}
			}
				
		});
		btnCalcular.setBounds(51, 214, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JButton btnLimpiar = new JButton("limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textNota1.setText("");
				textnota2.setText("");
				textnota3.setText("");
				
				lblResult.setText("");
				lblResult.setBackground(Color .ORANGE);
				lblimagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoArriba_32px.png")));
			}
		});
		btnLimpiar.setBounds(297, 214, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		lblimagen = new JLabel("");
		lblimagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoArriba_32px.png")));
		lblimagen.setBounds(310, 59, 54, 45);
		frame.getContentPane().add(lblimagen);
		
		textnota2 = new JTextField();
		textnota2.setBounds(127, 110, 86, 20);
		frame.getContentPane().add(textnota2);
		textnota2.setColumns(10);
		
		textnota3 = new JTextField();
		textnota3.setBounds(127, 159, 86, 20);
		frame.getContentPane().add(textnota3);
		textnota3.setColumns(10);
		
		
	}
}
