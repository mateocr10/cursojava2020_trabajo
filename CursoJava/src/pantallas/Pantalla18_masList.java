package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.DropMode;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JScrollPane;

public class Pantalla18_masList {

	private JFrame frame;
	private String values[]=new String[10];
	private String strTotal="";
	private JTextPane textPaneTablaResult;
	private int iSumaPar;
	private JScrollPane scrollPane;
	private JTextPane textPaneTablaDel2;
	private JTextPane textPaneTablaDel3;
	private JTextPane textPaneTabla4;
	private JTextPane textPaneTabla5;
	private JTextPane textPaneTabla6;
	private JTextPane textPaneTabla7;
	private JTextPane textPaneTabla_8;
	private JTextPane textPaneTabla9;
	private JTextPane textPaneTabla10;
	private JScrollPane scrollPane_3;
	private JScrollPane scrollPane_4;
	private JScrollPane scrollPane_5;
	private JScrollPane scrollPane_6;
	private JScrollPane scrollPane_7;
	private JScrollPane scrollPane_8;
	private JScrollPane scrollPane_9;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla18_masList window = new Pantalla18_masList();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla18_masList() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 795, 594);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas de multiplicar");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel.setBounds(255, 11, 296, 39);
		frame.getContentPane().add(lblNewLabel);
		
		String strListTabalas[] = new String [10];
		for(int i=0;i<10;i++){
			strListTabalas[i] = Integer.toString(i+1);
		}
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	
				for(int i=0;i<11;i++)
					strTotal+= 1+ "x" + i + "=" +(1*i)+ "\n"; 
				textPaneTablaResult.setText(strTotal);
			
				for(int i=0;i<11;i++)
					strTotal+= 2+ "x" + i + "=" +(2*i)+ "\n";
				textPaneTablaDel2.setText(strTotal);
				
				for(int i=0;i<11;i++)
					strTotal+= 3+ "x" + i + "=" +(3*i)+ "\n";
				textPaneTablaDel3.setText(strTotal);
				
				for(int i=0;i<11;i++)
					strTotal+= 4+ "x" + i + "=" +(4*i)+ "\n";
				textPaneTabla4.setText(strTotal);
				
				for(int i=0;i<11;i++)
					strTotal+= 5+ "x" + i + "=" +(5*i)+ "\n";
				textPaneTabla5.setText(strTotal);
				
				for(int i=0;i<11;i++)
					strTotal+= 6+ "x" + i + "=" +(6*i)+ "\n";
				textPaneTabla6.setText(strTotal);
				
				for(int i=0;i<11;i++)
					strTotal+= 7+ "x" + i + "=" +(7*i)+ "\n";
				textPaneTabla7.setText(strTotal);
				
				for(int i=0;i<11;i++)
					strTotal+= 8+ "x" + i + "=" +(8*i)+ "\n";
				textPaneTabla_8.setText(strTotal);
				
				for(int i=0;i<11;i++)
					strTotal+= 9+ "x" + i + "=" +(9*i)+ "\n";
				textPaneTabla9.setText(strTotal);
				
				for(int i=0;i<11;i++)
					strTotal+= 10+ "x" + i + "=" +(10*i)+ "\n";
				textPaneTabla10.setText(strTotal);
				
					
				
						
			}
		});
		btnCalcular.setBounds(241, 62, 310, 66);
		frame.getContentPane().add(btnCalcular);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(55, 150, 121, 186);
		frame.getContentPane().add(scrollPane);
		
		textPaneTablaResult = new JTextPane();
		scrollPane.setViewportView(textPaneTablaResult);
		textPaneTablaResult.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTablaResult.setEditable(false);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(192, 152, 119, 184);
		frame.getContentPane().add(scrollPane_1);
		
		textPaneTablaDel2 = new JTextPane();
		scrollPane_1.setViewportView(textPaneTablaDel2);
		textPaneTablaDel2.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTablaDel2.setEditable(false);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(332, 152, 119, 184);
		frame.getContentPane().add(scrollPane_2);
		
		textPaneTablaDel3 = new JTextPane();
		scrollPane_2.setViewportView(textPaneTablaDel3);
		textPaneTablaDel3.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTablaDel3.setEditable(false);
		
		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(467, 150, 121, 186);
		frame.getContentPane().add(scrollPane_3);
		
		textPaneTabla4 = new JTextPane();
		scrollPane_3.setViewportView(textPaneTabla4);
		textPaneTabla4.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTabla4.setEditable(false);
		
		scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(611, 151, 121, 186);
		frame.getContentPane().add(scrollPane_4);
		
		textPaneTabla5 = new JTextPane();
		scrollPane_4.setViewportView(textPaneTabla5);
		textPaneTabla5.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTabla5.setEditable(false);
		
		scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(55, 358, 121, 186);
		frame.getContentPane().add(scrollPane_5);
		
		textPaneTabla6 = new JTextPane();
		scrollPane_5.setViewportView(textPaneTabla6);
		textPaneTabla6.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTabla6.setEditable(false);
		
		scrollPane_6 = new JScrollPane();
		scrollPane_6.setBounds(192, 358, 121, 186);
		frame.getContentPane().add(scrollPane_6);
		
		textPaneTabla7 = new JTextPane();
		scrollPane_6.setViewportView(textPaneTabla7);
		textPaneTabla7.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTabla7.setEditable(false);
		
		scrollPane_7 = new JScrollPane();
		scrollPane_7.setBounds(332, 358, 121, 186);
		frame.getContentPane().add(scrollPane_7);
		
		textPaneTabla_8 = new JTextPane();
		scrollPane_7.setViewportView(textPaneTabla_8);
		textPaneTabla_8.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTabla_8.setEditable(false);
		
		scrollPane_8 = new JScrollPane();
		scrollPane_8.setBounds(467, 358, 121, 186);
		frame.getContentPane().add(scrollPane_8);
		
		textPaneTabla9 = new JTextPane();
		scrollPane_8.setViewportView(textPaneTabla9);
		textPaneTabla9.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTabla9.setEditable(false);
		
		scrollPane_9 = new JScrollPane();
		scrollPane_9.setBounds(611, 358, 121, 186);
		frame.getContentPane().add(scrollPane_9);
		
		textPaneTabla10 = new JTextPane();
		scrollPane_9.setViewportView(textPaneTabla10);
		textPaneTabla10.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTabla10.setEditable(false);
	}
}
 