package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.DefaultComboBoxModel;

public class Pantalla_ejr4 {

	private JFrame frame;
	private JLabel lblresultado;
	private JComboBox cmbCategoria;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_ejr4 window = new Pantalla_ejr4();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_ejr4() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		cmbCategoria = new JComboBox();
		cmbCategoria.setModel(new DefaultComboBoxModel(new String[] {"eliga categoria ", "a", "b", "c"}));
		cmbCategoria.setBounds(112, 11, 201, 32);
		frame.getContentPane().add(cmbCategoria);
		
		lblresultado = new JLabel("");
		lblresultado.setBackground(Color.ORANGE);
		lblresultado.setOpaque(true);
		lblresultado.setBounds(147, 93, 155, 39);
		frame.getContentPane().add(lblresultado);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int icategoria = cmbCategoria.getSelectedIndex();
				
				switch(icategoria){
				case 0:lblresultado.setText("eliga categoria");
					break;
				case 1: lblresultado.setText("hijo");
					break;
				case 2: lblresultado.setText("padres");
					break;
				case 3: lblresultado.setText("abuelos");
					break;
				}
				
				   
			}
		});
		btnNewButton.setBounds(147, 206, 149, 32);
		frame.getContentPane().add(btnNewButton);
	}
}
