package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.DropMode;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;

import org.omg.CORBA.portable.ValueInputStream;

import javax.swing.event.ListSelectionEvent;
import java.awt.Color;

public class Pantalla18 {

	private JFrame frame;
	private String values[]=new String[100];
	private String strTotal="";
	private JList list;
	private int acumuladorPar;
	//TODO agregar un label lblSupmaPar , agregar un acumulador iSynaPares
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla18 window = new Pantalla18();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla18() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 461, 413);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas de multiplicar");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel.setBounds(103, 33, 296, 39);
		frame.getContentPane().add(lblNewLabel);
		
		
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int acum= 0;
				for(int i=1; i<11;i++){
					values[acum]="tabla de" + i;
					
			
					
					
					for(int a=1; a<11; a++){
						
						values[acum]=i + "x" + a + "=" + (a*i);
						acum++;

						
						
					}
				
					
				}
				//se debe mostrar el resultado en el lbl
					
					
				
				list.setModel(new AbstractListModel() {
					
					
					public int getSize() {
						return values.length;
					}
					public Object getElementAt(int index) {
						return values[index];
					}
				});
			}
		});
			
		btnCalcular.setBounds(341, 112, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(83, 112, 193, 251);
		frame.getContentPane().add(scrollPane);
		
		list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				
			
				
			}
		});
		
		scrollPane.setViewportView(list);
	}
}
 