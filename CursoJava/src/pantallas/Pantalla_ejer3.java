package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.Color;

import javax.print.attribute.standard.Media;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Pantalla_ejer3 {

	private JFrame frame;
	private JLabel lblResult;
	private JComboBox cmbMeses;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_ejer3 window = new Pantalla_ejer3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_ejer3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		cmbMeses = new JComboBox();
		cmbMeses.setModel(new DefaultComboBoxModel(new String[] {"Meses", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"}));
		cmbMeses.setBounds(150, 52, 129, 20);
		frame.getContentPane().add(cmbMeses);
		
		lblResult = new JLabel("");
		lblResult.setFont(new Font("Tahoma", Font.PLAIN, 46));
		lblResult.setBackground(Color.MAGENTA);
		lblResult.setOpaque(true);
		lblResult.setBounds(81, 121, 278, 88);
		frame.getContentPane().add(lblResult);
		
		JButton btnNewButton = new JButton("calcular");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int imeses = cmbMeses.getSelectedIndex();
				switch (imeses) {
				case 0: lblResult.setText("eliga un mes");
					break;
				case 1: lblResult.setText("31");
					break;
				case 2:lblResult.setText("29");	
					break;
				case 3:lblResult.setText("31");
					break;
				case 4:lblResult.setText("30");
					break;
				case 5:lblResult.setText("31");
					break;
				case 6:lblResult.setText("30");
					break;
				case 7:lblResult.setText("31");
					break;
				case 8:lblResult.setText("31");
					break;
				case 9:lblResult.setText("30");
					break;
				case 10:lblResult.setText("31");
					break;
				case 11:lblResult.setText("30");
					break;
				case 12:lblResult.setText("31");		
					
					
					
				}
			}
		});
		btnNewButton.setBounds(160, 87, 89, 23);
		frame.getContentPane().add(btnNewButton);
	}
}
