package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.DropMode;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.Color;

public class Pantalla17 {

	private JFrame frame;
	private String values[]=new String[11];
	private String strTotal="";
	private JComboBox cmbTabla;
	private JList list;
	private JLabel lblFila;
	private JLabel lblSupmaPar;
	private int acumuladorPar;
	//TODO agregar un label lblSupmaPar , agregar un acumulador iSynaPares
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla17 window = new Pantalla17();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla17() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 461, 413);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas de multiplicar");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel.setBounds(103, 33, 296, 39);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblElijaTabla = new JLabel("Elija tabla");
		lblElijaTabla.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		lblElijaTabla.setBounds(45, 110, 91, 21);
		frame.getContentPane().add(lblElijaTabla);
		
		String strListTabalas[] = new String [10];
		for(int i=0;i<10;i++){
			strListTabalas[i] = Integer.toString(i+1);
		}
		cmbTabla = new JComboBox();
		
		cmbTabla.setModel(new DefaultComboBoxModel(strListTabalas));
		cmbTabla.setBounds(165, 113, 109, 20);
		frame.getContentPane().add(cmbTabla);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iTabla = Integer.parseInt((String)cmbTabla.getSelectedItem());
				for(int i=0; i<11;i++){
					values[i]=iTabla + "x" + i + "=" + (iTabla*i);
					int prod = iTabla*i;
					if(prod%2 ==0){
						acumuladorPar = acumuladorPar+prod;
					}
					lblSupmaPar.setText(Integer.toString(acumuladorPar));
					
				}
				//se debe mostrar el resultado en el lbl
					
					
				
				list.setModel(new AbstractListModel() {
					
					
					public int getSize() {
						return values.length;
					}
					public Object getElementAt(int index) {
						return values[index];
					}
				});
			}
		});
			
		btnCalcular.setBounds(341, 112, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(78, 159, 109, 153);
		frame.getContentPane().add(scrollPane);
		
		list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				int ifila = e.getLastIndex();
				lblFila.setText(values[ifila]);
				
			}
		});
		
		scrollPane.setViewportView(list);
		
		lblFila = new JLabel("");
		lblFila.setBackground(Color.MAGENTA);
		lblFila.setOpaque(true);
		lblFila.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblFila.setBounds(209, 159, 144, 62);
		frame.getContentPane().add(lblFila);
		
		lblSupmaPar = new JLabel("");
		lblSupmaPar.setBackground(Color.CYAN);
		lblSupmaPar.setOpaque(true);
		lblSupmaPar.setBounds(209, 259, 144, 53);
		frame.getContentPane().add(lblSupmaPar);
		
		JLabel lblNewLabel_1 = new JLabel("la suma de los pares es:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(209, 228, 171, 20);
		frame.getContentPane().add(lblNewLabel_1);
	}
}
 