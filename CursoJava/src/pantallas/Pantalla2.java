package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class Pantalla2 {

	private JFrame frame;
	private JLabel lblByteMax;
	private JLabel lblByteMin;
	private JLabel lblShortMax;
	private JLabel lblShortMin;
	private JLabel lblIntMax;
	private JLabel lblIntMin;
	private JLabel lblLongMax;
	private JLabel lblLongMin;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla2 window = new Pantalla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 691, 463);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("tipo de datos");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 40));
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel.setBounds(133, 11, 231, 49);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("tipo de datos");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(69, 99, 117, 25);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("maximo");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_2.setBounds(251, 99, 88, 25);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("minimo");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_3.setBounds(476, 99, 88, 25);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblByte = new JLabel("byte");
		lblByte.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblByte.setBounds(69, 147, 46, 14);
		frame.getContentPane().add(lblByte);
		
		lblByteMax = new JLabel("");
		lblByteMax.setFont(new Font("Tahoma", Font.ITALIC, 16));
		lblByteMax.setBackground(Color.GREEN);
		lblByteMax.setOpaque(true);
		lblByteMax.setBounds(251, 147, 156, 14);
		frame.getContentPane().add(lblByteMax);
		
		lblByteMin = new JLabel("");
		lblByteMin.setFont(new Font("Tahoma", Font.ITALIC, 16));
		lblByteMin.setOpaque(true);
		lblByteMin.setBackground(Color.GREEN);
		lblByteMin.setBounds(478, 147, 156, 14);
		frame.getContentPane().add(lblByteMin);
		
		
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setToolTipText("Sos el mejor!!");
		lblNewLabel_4.setIcon(new ImageIcon(Pantalla2.class.getResource("/iconos/medalla_oro_64px.png")));
		lblNewLabel_4.setBounds(407, 14, 64, 67);
		frame.getContentPane().add(lblNewLabel_4);
		
		JLabel lblShort = new JLabel("short");
		lblShort.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblShort.setBounds(69, 183, 46, 14);
		frame.getContentPane().add(lblShort);
		
		JLabel lblInt = new JLabel("int");
		lblInt.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblInt.setBounds(69, 219, 46, 14);
		frame.getContentPane().add(lblInt);
		
		JLabel lblNewLabel_7 = new JLabel("long");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_7.setBounds(69, 254, 26, 17);
		frame.getContentPane().add(lblNewLabel_7);
		
		lblShortMax = new JLabel("");
		lblShortMax.setFont(new Font("Tahoma", Font.ITALIC, 16));
		lblShortMax.setBackground(Color.GREEN);
		lblShortMax.setForeground(Color.BLACK);
		lblShortMax.setOpaque(true);
		lblShortMax.setBounds(251, 183, 156, 14);
		frame.getContentPane().add(lblShortMax);
		
		lblShortMin = new JLabel("");
		lblShortMin.setOpaque(true);
		lblShortMin.setForeground(Color.BLACK);
		lblShortMin.setFont(new Font("Tahoma", Font.ITALIC, 16));
		lblShortMin.setBackground(Color.GREEN);
		lblShortMin.setBounds(476, 185, 156, 14);
		frame.getContentPane().add(lblShortMin);
		
		lblIntMax = new JLabel("");
		lblIntMax.setOpaque(true);
		lblIntMax.setForeground(Color.BLACK);
		lblIntMax.setFont(new Font("Tahoma", Font.ITALIC, 16));
		lblIntMax.setBackground(Color.GREEN);
		lblIntMax.setBounds(251, 219, 156, 12);
		frame.getContentPane().add(lblIntMax);
		
		lblIntMin = new JLabel("");
		lblIntMin.setOpaque(true);
		lblIntMin.setForeground(Color.BLACK);
		lblIntMin.setFont(new Font("Tahoma", Font.ITALIC, 16));
		lblIntMin.setBackground(Color.GREEN);
		lblIntMin.setBounds(478, 221, 156, 14);
		frame.getContentPane().add(lblIntMin);
		
		lblLongMax = new JLabel("");
		lblLongMax.setOpaque(true);
		lblLongMax.setForeground(Color.BLACK);
		lblLongMax.setFont(new Font("Tahoma", Font.ITALIC, 12));
		lblLongMax.setBackground(Color.GREEN);
		lblLongMax.setBounds(251, 254, 156, 14);
		frame.getContentPane().add(lblLongMax);
		
		lblLongMin = new JLabel("");
		lblLongMin.setOpaque(true);
		lblLongMin.setForeground(Color.BLACK);
		lblLongMin.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblLongMin.setBackground(Color.GREEN);
		lblLongMin.setBounds(476, 257, 156, 14);
		frame.getContentPane().add(lblLongMin);
		JButton btnCalcular = new JButton("calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				byte bMin=Byte.MIN_VALUE;
				Byte bMin2=(byte)(Math.pow(2, 7));
				
				byte bMax=Byte.MAX_VALUE;
				Byte bMax2=(byte)(Math.pow(2, 7)-1);
				
				
				lblByteMax.setText(Byte.toString(bMax));
				lblByteMin.setText(Byte.toString(bMin));
				lblShortMax.setText("32767");
				lblShortMin.setText("-32768");
				lblIntMax.setText("2147483647");
				lblIntMin.setText("-21474836478");
				lblLongMax.setText("9223372036854775807");
				lblLongMin.setText("-9223372036854775808");
			}
		});
		btnCalcular.setBounds(69, 338, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JButton btnLImpiar = new JButton("Limpiar");
		btnLImpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblByteMax.setText("");
				lblByteMin.setText("");
				lblShortMax.setText("");
				lblShortMin.setText("");
				lblIntMax.setText("");
				lblIntMin.setText("");
				lblLongMax.setText("");
				lblLongMin.setText("");
			}
		});
		btnLImpiar.setBounds(517, 337, 117, 25);
		frame.getContentPane().add(btnLImpiar);
		
	}
}
