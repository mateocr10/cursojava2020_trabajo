package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GuessANumber {
	
	private JFrame frame;
	private JTextField textNumeroelegido;
	private JLabel lblArribaAbajo;
	private JLabel lblElegido;
	private JLabel labelTextoArribaAbajo;
	private JLabel lblCantidaddeintendos;
	private int cantidadIntentos;//va a la cantidad contar intentos

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuessANumber window = new GuessANumber();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuessANumber() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 572, 384);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTitulo = new JLabel("el elegido es");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblTitulo.setBounds(26, 166, 138, 19);
		frame.getContentPane().add(lblTitulo);
		
		JLabel lblEligeunnumero = new JLabel("Elige un numero ");
		lblEligeunnumero.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblEligeunnumero.setBounds(163, 11, 227, 37);
		frame.getContentPane().add(lblEligeunnumero);
		
		JLabel lblEligeunnumero1 = new JLabel("elige un numero entre 1 y 99");
		lblEligeunnumero1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEligeunnumero1.setBounds(26, 118, 259, 25);
		frame.getContentPane().add(lblEligeunnumero1);
		
		textNumeroelegido = new JTextField();
		textNumeroelegido.setBounds(351, 124, 86, 20);
		frame.getContentPane().add(textNumeroelegido);
		textNumeroelegido.setColumns(10);
		
		lblArribaAbajo = new JLabel("");
		lblArribaAbajo.setVisible(false);
		lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoArriba_32px.png")));
		lblArribaAbajo.setBounds(203, 200, 32, 32);
		frame.getContentPane().add(lblArribaAbajo);
		
		labelTextoArribaAbajo = new JLabel("mas alto");
		labelTextoArribaAbajo.setVisible(false);
		labelTextoArribaAbajo.setBackground(Color.YELLOW);
		labelTextoArribaAbajo.setOpaque(true);
		labelTextoArribaAbajo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		labelTextoArribaAbajo.setBounds(64, 196, 75, 36);
		frame.getContentPane().add(labelTextoArribaAbajo);
		
		lblElegido = new JLabel("");
		lblElegido.setBackground(Color.GREEN);
		lblElegido.setOpaque(true);
		lblElegido.setBounds(351, 159, 86, 29);
		lblElegido.setVisible(false);
		
		int iNumeroElegido = (int)(Math.random()*1000%100);
		lblElegido.setText(Integer.toString(iNumeroElegido)); 
		frame.getContentPane().add(lblElegido);
		
		JLabel lblNewLabel = new JLabel("Cantidad de intentos ");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(39, 79, 138, 19);
		frame.getContentPane().add(lblNewLabel);
		
		lblCantidaddeintendos = new JLabel("");
		lblCantidaddeintendos.setBackground(Color.ORANGE);
		lblCantidaddeintendos.setOpaque(true);
		lblCantidaddeintendos.setBounds(213, 72, 58, 25);
		frame.getContentPane().add(lblCantidaddeintendos);
		
		JButton btnAceptar = new JButton("aceptar ");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				int iMiValor = Integer.parseInt(textNumeroelegido.getText());
				int ivalorCompu =  Integer.parseInt(lblElegido.getText());
				if(iMiValor>ivalorCompu){
									
					labelTextoArribaAbajo.setText("mas abajo");
					labelTextoArribaAbajo.setVisible(true);
					
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoAbajo_32px.png")));
					lblArribaAbajo.setVisible(true);
				
				}else if(iMiValor<ivalorCompu){					
					
					labelTextoArribaAbajo.setText("mas arriba");
					labelTextoArribaAbajo.setVisible(true);
					
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoArriba_32px.png")));
					lblArribaAbajo.setVisible(true);
				}else{
					labelTextoArribaAbajo.setText("le pegaste");
					labelTextoArribaAbajo.setVisible(true);
					
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/correcto_32px.png")));
					lblArribaAbajo.setVisible(true);
					lblElegido.setVisible(true);
				}
				cantidadIntentos++;
				lblCantidaddeintendos.setText(Integer.toString(cantidadIntentos));
				
			}
		});
		btnAceptar.setBounds(457, 123, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		JButton btnLimpiar = new JButton("limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cantidadIntentos = 0;
				
				textNumeroelegido.setText("");
				lblCantidaddeintendos.setText("");
												
				labelTextoArribaAbajo.setVisible(false);
				lblArribaAbajo.setVisible(false);
			}
		});
		btnLimpiar.setBounds(457, 165, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
	}
}
