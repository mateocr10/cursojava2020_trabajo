package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Color;

import javax.print.attribute.IntegerSyntax;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class pantalla4 {

	private JFrame frame;
	private JTextField textNumero1;
	private JLabel lblNumero;
	private JLabel lblResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pantalla4 window = new pantalla4();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public pantalla4() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		frame.setBounds(100, 100, 618, 427);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u00BFPar o impar?");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNewLabel.setBounds(200, 11, 182, 37);
		frame.getContentPane().add(lblNewLabel);
		
		lblNumero = new JLabel("numero");
		lblNumero.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNumero.setBounds(36, 165, 68, 25);
		frame.getContentPane().add(lblNumero);
		
		textNumero1 = new JTextField();
		textNumero1.setFont(new Font("Tahoma", Font.PLAIN, 23));
		textNumero1.setBounds(145, 162, 125, 38);
		frame.getContentPane().add(textNumero1);
		textNumero1.setColumns(10);
		
		lblResultado = new JLabel("");
		lblResultado.setBackground(Color.YELLOW);
		lblResultado.setForeground(Color.BLACK);
		lblResultado.setOpaque(true);
		lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblResultado.setBounds(312, 147, 211, 53);
		frame.getContentPane().add(lblResultado);
		
		JButton btnCalcular = new JButton("calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int inumero = Integer.parseInt(textNumero1.getText());
				
				int iparidad = inumero%2;
				if(iparidad == 0){
					lblResultado.setText("es par");
				}
				else{
					lblResultado.setText("es impar");
				}			
				if(inumero == 0){
					lblResultado.setText("0 no es par ni\n impar no seas rompe huevos");
				}
			}
		});
		btnCalcular.setBounds(36, 286, 125, 37);
		frame.getContentPane().add(btnCalcular);
		
		JButton btnLimpiar = new JButton("limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int inumero = Integer.parseInt(textNumero1.getText());
				
				int iparidad = inumero%2;
				if(iparidad == 0){
					lblResultado.setText("");
				}
				else{
					lblResultado.setText("");
				}			
				
				
			}
		});
		btnLimpiar.setBounds(414, 286, 125, 37);
		frame.getContentPane().add(btnLimpiar);
		
	}
}
